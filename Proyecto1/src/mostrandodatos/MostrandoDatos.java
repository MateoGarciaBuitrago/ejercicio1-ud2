package mostrandodatos;

import java.util.Scanner;

public class MostrandoDatos {

	public static void main(String[] args) {
		Scanner lector = new Scanner (System.in);
		System.out.println("Introduce tu nombre");
		String nombre = lector.nextLine();
		
		System.out.println("Introduce tu apellido");
		String apellido = lector.nextLine();
		
		System.out.println(nombre+" "+apellido);
		
		
		lector.close();
	}

}
